# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.0.1] - 2022-03-23

### Added

- Module added

[0.0.1]: https://bitbucket.org/martin_kedmenec/visma-module-certification-module-m2/src/0.0.1/
